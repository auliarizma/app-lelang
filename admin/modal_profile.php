
<div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable" role="document">
              <div class="modal-content">
                <div class="modal-header">
                <?php
         
            $profile = mysqli_query($koneksi, "SELECT * FROM users WHERE id_user='$_SESSION[id_user]'");
            $p=mysqli_fetch_array($profile);
          ?>
                  <h5 class="modal-title" id="exampleModalScrollableTitle">Profile <?=$p['role']?></h5>
                  
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">





    <table class='table table-striped'>
        <tr>
            <th width="20%">Nama</th>
            <td width="1%">:</td>
            <td><?= $p['nama'] ?></td>
        </tr>
        <tr>
            <th>Email</th>
            <td>:</td>
            <td><?= $p['email'] ?></td>
        </tr>
        <tr>
            <th>Nomor HP</th>
            <td>:</td>
            <td><?= $p['no_hp'] ?></td>
        </tr>
        <tr>
            <th>Alamat</th>
            <td>:</td>
            <td><?= $p['alamat'] ?></td>
        </tr>
        <tr>
            <th width="20%">Foto Profile</th>
            <td width="1%">:</td>
            
            
            
                <td><img class="img-profile rounded-circle" src="img/<?= $p['foto'] ?>" style="max-width: 60px"></td> 
                
        </tr>
        
    </table>
    <br>
    <h5 class="modal-title" id="exampleModalScrollableTitle">Update Profile <?=$p['role']?></h5>
    
<form method="POST" action="action_update_profile.php" enctype="multipart/form-data">


    <div class="form-group">
        <label for="kategori">Nama</label>
        <input type="text" class="form-control" name="nama" id="kategori" value="<?= $p['nama'] ?>" placeholder="Masukkan Nama">
        <input type="hidden" class="form-control" name="id_user" id="kategori" value="<?= $p['id_user'] ?>" placeholder="Masukkan Nama">
    </div>



    <div class="form-group">
        <label for="kategori">Email</label>
        <input type="email" class="form-control" name="email" id="kategori" value="<?= $p['email'] ?>" placeholder="Masukkan Kategori">
    </div>


    <div class="form-group">
        <label for="kategori">Password</label>
        <input type="password" class="form-control" name="password" id="kategori" placeholder="Masukkan Password">
    </div>

    <div class="form-group">
        <label for="harga_buka">No Hp</label>
        <input type="number" class="form-control" name="no_hp" id="harga_buka" value="<?= $p['no_hp'] ?>" placeholder="Masukkan Harga Buka" required>
    </div>
    <div class="form-group">
        <label for="keterangan">Alamat</label>
        <textarea type="text" class="form-control" name="alamat" id="keterangan" placeholder="Masukkan Keterangan" required><?= $p['alamat'] ?></textarea>
    </div>
    <div class="form-group">
        <label for="barang">Foto </label>
        <input type="file" class="form-control" name="foto" id="barang">
        <p class="help-block">*Format JPG/PNG.</p>
    </div>
    

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
              </div>
            </div>
          </div>


          <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLogout"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabelLogout">Ohh No!</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Yakin Mau Keluar?</p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
                  <a href="logout.php" class="btn btn-primary">Logout</a>
                </div>
              </div>
            </div>
          </div>

       